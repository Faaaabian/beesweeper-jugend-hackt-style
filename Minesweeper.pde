public int size = 800;
public int w = 40;
public Cell[][] grid;
public int cols = floor(size/w);
public int rows = floor(size/w);
public int totalBombs = floor(cols*rows*0.1);
public int unrevealed;
public boolean gameOver = false;
public PImage alpakared;
public PImage alpakablue;

public Cell[][] createGrid(int cols, int rows) {
  return new Cell[cols][rows];
}

void setup() {
  size(800, 800);
  alpakared = loadImage("alpakared.png");
  alpakablue = loadImage("Alpaka.png");
  restart();
}

void mousePressed() {
  for (int i = 0; i < cols; i++) {
    for ( int j = 0; j < rows; j++) {
      if (grid[i][j].contained(mouseX, mouseY)) {
        grid[i][j].reveal();
        if(grid[i][j].bee()){
          gameOver();
        }
      }
    }
  }
}

void keyPressed(){
  if(key == ' ' && gameOver){
    restart();
  }
}

void gameOver(){
  gameOver = true;
  for (int i = 0; i < cols; i++) {
    for ( int j = 0; j < rows; j++) {
      grid[i][j].reveal();
      grid[i][j].show();
    }
  }
}

void win(){
  gameOver();
  delay(1000);
  fill(255);
  textAlign(CENTER);
  textSize(40);
  image(alpakablue, 0, 0, width, height);
  text("YOU WON\nPress Space to play again", width/2, height/4);
}

void lose(){
  gameOver();
  delay(1000);
  fill(255);
  textAlign(CENTER);
  textSize(40);
  image(alpakared, 0, 0, width, height);
  text("YOU LOST\nPress Space to play again", width/2, height/4);
}

void restart(){
  gameOver = false;
  background(250, 155, 3);
  unrevealed = cols*rows;
  grid = createGrid(cols, rows);
  for (int i = 0; i < cols; i++) {
    for ( int j = 0; j < rows; j++) {
      grid[i][j] = new Cell(i, j, false);
    }
  }

  ArrayList<PVector> bees = new ArrayList<PVector>();
  PVector spot;
  
  for (int n = 0; n < totalBombs; n++) {
    do {
      spot = new PVector(random(cols), random(rows));
    } while (bees.contains(spot));
    bees.add(spot);
  }
  

  for (PVector current : bees){
    grid[int(current.x)][int(current.y)] = new Cell(int(current.x), int(current.y), true);
  }
  
  for (int i = 0; i < cols; i++) {
    for ( int j = 0; j < rows; j++) {
      grid[i][j].calculateNeighbors();
    }
  }
  delay(500);
}

void draw() {
  for (int i = 0; i < cols; i++) {
    for ( int j = 0; j < rows; j++) {
      grid[i][j].show();
    }
  }
  if(unrevealed == totalBombs) win();
  if(gameOver)lose();
}
