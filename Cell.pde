public class Cell {
  private int x, y, i, j, neighbors;
  private boolean bee, revealed;
  public Cell(int _x, int _y, boolean _bee) {
    this.x = _x;
    this.y = _y;
    this.i = this.x *w;
    this.j = this.y*w;
    this.bee = _bee;
    this.revealed = false;
  }

  public void show() {
    stroke(0);
    strokeWeight(1);
    fill(250, 155, 3);
    rect(this.x*w, this.y*w, this.x*w+w, this.y*w+w);

    if (revealed) {
      fill(254, 213, 48);
      rect(this.x*w, this.y*w, this.x*w+w, this.y*w+w);
      if (bee) {
        image(alpakared, this.i, this.j, w, w);

        //fill(150);
        //ellipseMode(RADIUS);
        //ellipse(this.x*w +w/2, this.y*w+ w/2, w*0.25, w*0.25);
      } else if (neighbors == 0) {
        fill(255);
      } else {
        fill(0);
        textAlign(CENTER);
        textSize(w/2);
        stroke(0);
        text(neighbors, this.x*w+w/2, this.y*w+w/2+w/4);
      }
    }
  }

  public boolean contained(int x, int y) {
    return (x > this.x*w && x < this.x*w+w && y > this.y*w && y < this.y*w+w);
  }

  public void reveal() {
    this.revealed = true;
    unrevealed--;
    if (this.neighbors == 0) {
      floodfill();
    }
  }

  public void calculateNeighbors() {
    if (this.bee) {
      this.neighbors = -1;
      return;
    }
    int nbs = 0;
    for (int xoff = -1; xoff < 2; xoff++) {
      int celli = x + xoff;
      if (celli < 0 || celli >= cols) continue;
      for (int yoff = -1; yoff <= 1; yoff++) {
        int cellj = y + yoff;
        if (cellj < 0 || cellj >= rows) continue;

        Cell neighbor = grid[celli][cellj];
        if (neighbor.bee()) {
          nbs++;
        }
      }
      this.neighbors = nbs;
    }
  }

  public boolean bee() {
    return this.bee;
  }

  public void floodfill() {
    for (int xoff = -1; xoff <= 1; xoff++) {
      int celli = x + xoff;
      if (celli < 0 || celli >= cols) continue;

      for (int yoff = -1; yoff <= 1; yoff++) {
        int cellj = y + yoff;
        if (cellj < 0 || cellj >= rows) continue;

        Cell neighbor = grid[celli][cellj];
        if (!neighbor.revealed) {
          neighbor.reveal();
        }
      }
    }
  }
}
